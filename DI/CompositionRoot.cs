﻿
using BusinessLayerMessageSaver;
using BusinessLayerMessageSaver.Helpers;
using DataAccess;
using Interfaces;
using Unity;

namespace DI
{
    public class CompositionRoot
    {
        public static IUnityContainer Container { get; }

        static CompositionRoot()
        {
            Container = new UnityContainer();
        }

        public static void Register()
        {
            Container.RegisterType<IDataProvider, DataProvider>();
            Container.RegisterType<IMessageSaverModel, MessageSaverModel>();
            Container.RegisterType<ITelegramBotWrapper, TelegramBotWrapper>();
            Container.RegisterType<ICommander, Commander>();
        }
        public static IMessageSaverModel GetModel()
        {
          return  Container.Resolve<IMessageSaverModel>();            
        }
    }
}