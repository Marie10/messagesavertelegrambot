﻿using MessageSaver.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace MessageSaver.Commands
{
    public abstract class ListCommand 
    {
        protected readonly DataProvider _dataProvider;
     
        public List<string> folderList { get; set; }
        public bool inProgress { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Update userAnswer { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ListCommand(DataProvider dataProvider)
        {       
            _dataProvider = dataProvider;
        }
        public ReplyKeyboardMarkup ShowList()
        {
            folderList = _dataProvider.SelectFolderList();
            var replyKeyboard = new ReplyKeyboardMarkup();
            var rows =new List<KeyboardButton[]>();
            var cols = new List<KeyboardButton>();
            for (int i =0;i< folderList.Count;i++)
            {
                cols.Add(new KeyboardButton(folderList[i]));
                if (i % 2 != 0)
                {
                    rows.Add(cols.ToArray());
                    cols = new List<KeyboardButton>();
                }
          }          
            replyKeyboard.Keyboard = rows.ToArray();
            replyKeyboard.OneTimeKeyboard = true;
            return replyKeyboard;
        }
        
    }
}
