﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace MessageSaver.Commands
{
  public  interface ICommand
    {
         bool inProgress { get; set; }
         Update userAnswer { get; set; }
         void Done(Command command, Update message);
         string GetInstruction();

    }
}
