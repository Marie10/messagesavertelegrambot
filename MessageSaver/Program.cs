﻿using DI;
using Interfaces;
using System;
using System.Threading;

namespace MessageSaver
{
    class Program
    {
        static void Main(string[] args)
        {

            CompositionRoot.Register();
            var bot = CompositionRoot.GetModel();
            Thread th = new Thread(bot.StartReceiving);
            th.Start();
            Console.ReadLine();
        }
    }
}
