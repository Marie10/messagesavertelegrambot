﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayerMessageSaver.Helpers;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class CommanderTests
    {
        private Commander _commander;
        [SetUp]
        public void SetUp()
        {
            _commander =  new Commander();
        }

        [Test]
        public void IsCommand_ReturnTrue_WhenTextIsCommand()
        {
            string command = "/somecommand";
            var result =  _commander.IsCommand(command);
            Assert.AreEqual(result,true);
        }

        [Test]
        public void IsCommand_ReturnFaslse_WhenTextNotCommand()
        {
            string command = "incorrectCommand";
            var result = _commander.IsCommand(command);
            Assert.AreEqual(result, false);
        }
    }
}
