﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Interfaces
{
    public interface IDataProvider
    {
         List<string> SelectFolderList();
        void InsertNewFolderToTable(Message message);
    }
}
