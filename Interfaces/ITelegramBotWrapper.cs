﻿
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Interfaces
{
    public interface ITelegramBotWrapper
    {
        TelegramBotClient TelegaBot { get; }
        Task SendBotAnswer(long chatId, string answerFromBot);
        Task SendMenuToUser(int chantId, ReplyKeyboardMarkup menu);
    }
}
