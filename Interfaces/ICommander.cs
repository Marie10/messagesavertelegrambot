﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
  public interface ICommander
  {
      bool IsCommand(string commandText);
      bool IsDoneCurrentCommand(string text);
  }
}
