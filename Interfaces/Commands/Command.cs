﻿

namespace Interfaces.Commands
{
    public class Command
    {
        public ICommand CurrentCommand { get; set; }
        public Command(ICommand currentCommand)
        {
            CurrentCommand = currentCommand;
        }


        public void Done()
        {
            CurrentCommand.Done(this);
        }

    }
}
