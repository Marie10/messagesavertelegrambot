﻿using Telegram.Bot.Types;

namespace Interfaces.Commands
{
  public  interface ICommand
    {
         bool InProgress { get; set; }
         Message UserAnswer { get; set; }
        void Done(Command command);
        string GetInstruction();
    }
}
