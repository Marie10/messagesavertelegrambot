﻿using BusinessLayerMessageSaver.Commands;
using BusinessLayerMessageSaver.Helpers;
using Interfaces;
using Interfaces.Commands;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace BusinessLayerMessageSaver
{
   public class MessageSaverModel: IMessageSaverModel
    {
        private readonly ITelegramBotWrapper _messageSaver;
        private readonly IDataProvider _dataProvider;
        private readonly ICommander _commander;
        private  Command _command;

        public MessageSaverModel(ITelegramBotWrapper messageSaver, IDataProvider dataProvider, ICommander commander)
        {
            _messageSaver = messageSaver;
            _dataProvider = dataProvider;
            _commander = commander;
            _messageSaver.TelegaBot.OnMessage += BotOnMessageReceived;
            _command = new Command(new EmptyCommand());
        }
        public  async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var latestMessage = messageEventArgs.Message;

            if (_commander.IsCommand(latestMessage.Text))
            {
                ManageCommand(latestMessage.Text);
                await _messageSaver.SendBotAnswer(latestMessage.Chat.Id, _command.CurrentCommand.GetInstruction());
            }
            if (_command.CurrentCommand.InProgress)
            {
                _command.CurrentCommand.UserAnswer = latestMessage;
            }
        }
      
        private void HandleDoneForCurrentCommand(string commandText)
        {
            if (_command.CurrentCommand.GetType().IsInstanceOfType(typeof(EmptyCommand)))
            {
                _command.Done();
            }
        }

        private async void ManageCommand(string commandText)
        {
            if (commandText == Commander.CommandNewFolder)
            {
                _command = new Command(new NewFolderCommand(_dataProvider));
            }

            if (commandText == Commander.CommandSetItem)
            {
                _command = new Command(new SetItemComand(_dataProvider));

                if (_command.CurrentCommand is SetItemComand)
                {
                    //TODO: GetChatID;
                    var listCommand = (SetItemComand)_command.CurrentCommand;
                    var foldersList = listCommand.ShowList();
                    await _messageSaver.SendMenuToUser(386400769, foldersList);
                    //_bot.SendTextMessageAsync(386400769, "Test", false, false, 0,0,keyBoard, Telegram.Bot.Types.Enums.ParseMode.Default);
                }
            }
            //TODO fix EmprtyCommand
            if (_commander.IsDoneCurrentCommand(commandText))
            {
                _command.Done();
                //HandleDoneForCurrentCommand(commandText);
            }
        }
             
        public void StartReceiving()
        {
            _messageSaver.TelegaBot.StartReceiving();
        }     
    }
}
