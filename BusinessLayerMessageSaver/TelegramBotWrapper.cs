﻿
using System;
using Interfaces;
using Telegram.Bot.Types;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using System.Threading.Tasks;
using Telegram.Bot.Args;

namespace BusinessLayerMessageSaver
{
    public class TelegramBotWrapper : ITelegramBotWrapper
    {
        private TelegramBotClient _telegaBot;

        public TelegramBotWrapper()
        {

            _telegaBot =  new TelegramBotClient("789322371:AAGk7-gLsHU64f0ZO5Ss5zROtFiMLiAYw9I");          
        }

        public TelegramBotClient TelegaBot
        {
            get
            {
                return _telegaBot;
            }
        }

     
        public Task SendBotAnswer(long chatId, string answerFromBot)
        {
          return TelegaBot.SendTextMessageAsync(chatId, answerFromBot);
        }

        public Task SendMenuToUser(int chantId, ReplyKeyboardMarkup menu)
        {
           return TelegaBot.SendTextMessageAsync(386400769, "Test", ParseMode.Default, false, false, 0, menu);
        }
    }
}
