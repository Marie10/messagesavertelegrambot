﻿using System.Text.RegularExpressions;
using Interfaces;
using Telegram.Bot.Types;

namespace BusinessLayerMessageSaver.Helpers
{
    public class Commander : ICommander
    {
        public const string CommandNewFolder = "/createnewfolder";
        public const string CommandDone = "/done";
        public const string CommandSetItem = "/setitem";

        public  bool IsCommand(string commandText)
        {
            return Regex.IsMatch(commandText, @"^[/][a-z]+$", RegexOptions.None | RegexOptions.None);
        }

        public  bool IsDoneCurrentCommand(string text)
        {
            return text == CommandDone;
        }
    }
}
