﻿using Interfaces;
using Interfaces.Commands;
using Telegram.Bot.Types;

namespace BusinessLayerMessageSaver.Commands
{
    public  class NewFolderCommand : ICommand
    {
        private readonly IDataProvider _dataProvider;
        public bool InProgress { get; set; }
        public Message UserAnswer { get; set; }
        public NewFolderCommand()
        {
        }
        public NewFolderCommand(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
            InProgress = true;
            UserAnswer = new Message();
        }

        public void Done(Command command)
        {
            _dataProvider.InsertNewFolderToTable(UserAnswer);
            InProgress = false;
            command.CurrentCommand = new EmptyCommand();
        }

        public string GetInstruction()
        {
            return "Input folder name and set /done to finish command ";
        }
    }
}
