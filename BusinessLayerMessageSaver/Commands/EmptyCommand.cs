﻿using Interfaces.Commands;
using Telegram.Bot.Types;

namespace BusinessLayerMessageSaver.Commands
{
 public class EmptyCommand : ICommand
    {
        public bool InProgress { get; set; }
        public Message UserAnswer { get; set; }

        public EmptyCommand()
        {
            InProgress = false;
            UserAnswer = new Message();
        }
        public void Done(Command command)
        {
            command.CurrentCommand = new EmptyCommand();
        }

        public string GetInstruction()
        {
            return "Please choose some command before set done to action";
        }
    }
}

