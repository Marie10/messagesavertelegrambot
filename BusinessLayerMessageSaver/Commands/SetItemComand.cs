﻿using System;
using Interfaces;
using Interfaces.Commands;
using Telegram.Bot.Types;

namespace BusinessLayerMessageSaver.Commands
{
    public class SetItemComand :ListCommand, ICommand
    {
        public SetItemComand(IDataProvider dataProvider) : base(dataProvider)
        {


        }
        public void ShowFolders()
        {
            ShowList();
        }

        public bool InProgress { get; set; }
        public Update UserAnswer { get; set; }

        public void Done(Command command)
        {
            throw new NotImplementedException();
        }

        public string GetInstruction()
        {
            return "Send message that you want to save in folder";
        }
    }
}
