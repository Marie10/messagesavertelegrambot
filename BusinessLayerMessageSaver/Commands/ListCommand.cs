﻿using System.Collections.Generic;
using Interfaces;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BusinessLayerMessageSaver.Commands
{
    public abstract class ListCommand 
    {         
        public bool InProgress { get; set; }
        public Message UserAnswer { get; set; }

        private readonly IDataProvider _dataProvider;
        private List<string> _folderList;

        public ListCommand(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }
        public ReplyKeyboardMarkup ShowList()
        {
            _folderList = _dataProvider.SelectFolderList();
            _folderList = new List<string>{"Hi","Test", "Menu"};
            var replyKeyboard = new ReplyKeyboardMarkup();
            var rows =new List<KeyboardButton[]>();
            var cols = new List<KeyboardButton>();
            for (int i =0;i< _folderList.Count;i++)
            {
                cols.Add(new KeyboardButton(_folderList[i]));
                if (i % 2 != 0)
                {
                    rows.Add(cols.ToArray());
                    cols = new List<KeyboardButton>();
                }
            }
                      
            replyKeyboard.Keyboard = rows.ToArray();
            replyKeyboard.OneTimeKeyboard = true;
            return replyKeyboard;
        }       
    }
}
